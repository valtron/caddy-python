package python

import (
	"testing"
	"github.com/stretchr/testify/assert"
	
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func TestSetup(t *testing.T) {
	input := "python /foo"
	c := caddy.NewTestController("http", input)
	err := setup(c)
	
	if err != nil {
		t.Errorf("setup() returned err: %v", err)
	}
	
	mids := httpserver.GetConfig(c).Middleware()
	mid := mids[len(mids)-1]
	handler := mid(nil).(Handler)
	
	expected := []*Config {
		&Config {
			Path: "/foo",
			Except: []string{},
		},
	}
	
	assert.Equal(t, expected, handler.Configs)
}
