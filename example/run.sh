set -e

trap on_exit EXIT

function on_exit() {
	kill -2 $PID_CADDY
}

#go install github.com/mholt/caddy/caddy
$GOPATH/bin/caddy -log stderr -conf Caddyfile &
PID_CADDY=$!

curl 127.0.0.1:8000/foo/bar
curl 127.0.0.1:8000/robots.txt
