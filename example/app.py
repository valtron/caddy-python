import sys
from wsgiref.simple_server import make_server

def main(port):
	port = int(port)
	with make_server('127.0.0.1', port, simple_app) as httpd:
		print("Serving on port {}...".format(port))
		httpd.serve_forever()

def simple_app(environ, start_response):
	status = '200 OK'
	headers = [('Content-type', 'text/plain; charset=utf-8')]
	
	start_response(status, headers)
	
	return [
		b'=== Response ===\n',
		*(
			('{}: {}\n'.format(key, environ.get(key)).encode('utf-8'))
			for key in ['REQUEST_METHOD', 'PATH_INFO', 'QUERY_STRING']
		),
		b'================\n'
	]

if __name__ == '__main__':
	main(*sys.argv[1:])
