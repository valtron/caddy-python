package python

import (
	"log"
	"errors"
	"strings"
	"net"
	"net/http"
	"net/url"
	"os/exec"
	"strconv"
	"sync/atomic"
	"github.com/mholt/caddy/caddyhttp/httpserver"
	"github.com/mholt/caddy/caddyhttp/proxy"
)

type Handler struct {
	Next    httpserver.Handler
	Proxies []*PythonProxy
}

type PythonProxy struct {
	Path string
	Module string
	WithoutPrefix string
	Except []string
	UseVenv bool
	Dir string
	PythonBin string
	Processes []*Process
	MaxProcesses int
}

func (p *PythonProxy) Close() {
	for _, proc := range p.Processes {
		proc.Close()
	}
}

type Process struct {
	impl *exec.Cmd
	Port int
	Conns int32
	MaxConns int32
	ReverseProxy *proxy.ReverseProxy
}

func (p *Process) Close() {
	if p.impl != nil {
		p.impl.Process.Kill()
	}
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) (int, error) {
	upstream := h.match(r)
	if upstream == nil {
		return h.Next.ServeHTTP(w, r)
	}
	
	outreq := createUpstreamRequest(r)
	
	var backendErr error
	for {
		proc, err := upstream.Select(r)
		
		if err != nil {
			backendErr = err
			break
		}
		
		rproxy := proc.ReverseProxy
		
		if rproxy == nil {
			return http.StatusInternalServerError, errors.New("process missing proxy")
		}
		
		outreq.Host = LOCALHOST
		
		func() {
			atomic.AddInt32(&proc.Conns, 1)
			defer atomic.AddInt32(&proc.Conns, -1)
			backendErr = rproxy.ServeHTTP(w, outreq, nil)
		}()
		
		if backendErr == nil {
			return 0, nil
		}
		
		if _, ok := backendErr.(httpserver.MaxBytesExceeded); ok {
			return http.StatusRequestEntityTooLarge, backendErr
		}
		
		break
	}
	
	log.Printf("[ERROR] %s", backendErr)
	
	return http.StatusBadGateway, backendErr
}

func (p *PythonProxy) Select(r *http.Request) (*Process, error) {
	pool := p.Processes
	
	for _, proc := range pool {
		if proc.Conns < proc.MaxConns {
			return proc, nil
		}
	}
	
	if len(pool) < p.MaxProcesses {
		// Start new process
		port := GetFreePort()
		if port < 0 {
			return nil, errors.New("no free ports available")
		}
		port_str := strconv.Itoa(port)
		cmd := exec.Command(p.PythonBin, p.Module, port_str)
		err := cmd.Start()
		if err != nil {
			return nil, err
		}
		
		nameURL, _ := url.Parse("http://" + LOCALHOST + ":" + port_str)
		rproxy := proxy.NewSingleHostReverseProxy(nameURL, p.WithoutPrefix, 0)
		
		proc := &Process {
			impl: cmd, Port: port, Conns: 0, MaxConns: 1, ReverseProxy: rproxy,
		}
		p.Processes = append(p.Processes, proc)
		return proc, nil
	}
	
	return nil, errors.New("no processes available upstream")
}

func GetFreePort() int {
	addr, err := net.ResolveTCPAddr("tcp", LOCALHOST + ":0")
	if err != nil {
		return -1
	}
	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return -1
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port
}

func (h Handler) match(r *http.Request) *PythonProxy {
	var p *PythonProxy
	var longestMatch int
	
	rp := httpserver.Path(r.URL.Path)
	
	for _, proxy := range h.Proxies {
		basePath := proxy.Path
		
		if !rp.Matches(basePath) {
			continue
		}
		excluded := false
		for _, ex_path := range proxy.Except {
			if rp.Matches(ex_path) {
				excluded = true
				break
			}
		}
		if excluded {
			continue
		}
		
		if len(basePath) > longestMatch {
			longestMatch = len(basePath)
			p = proxy
		}
	}
	return p
}

const LOCALHOST = "127.0.0.1"

//
// Code after this is copied from Caddy
//

func createUpstreamRequest(r *http.Request) *http.Request {
	outreq := new(http.Request)
	*outreq = *r // includes shallow copies of maps, but okay
	// We should set body to nil explicitly if request body is empty.
	// For server requests the Request Body is always non-nil.
	if r.ContentLength == 0 {
		outreq.Body = nil
	}

	// Restore URL Path if it has been modified
	if outreq.URL.RawPath != "" {
		outreq.URL.Opaque = outreq.URL.RawPath
	}

	// We are modifying the same underlying map from req (shallow
	// copied above) so we only copy it if necessary.
	copiedHeaders := false

	// Remove hop-by-hop headers listed in the "Connection" header.
	// See RFC 2616, section 14.10.
	if c := outreq.Header.Get("Connection"); c != "" {
		for _, f := range strings.Split(c, ",") {
			if f = strings.TrimSpace(f); f != "" {
				if !copiedHeaders {
					outreq.Header = make(http.Header)
					copyHeader(outreq.Header, r.Header)
					copiedHeaders = true
				}
				outreq.Header.Del(f)
			}
		}
	}

	// Remove hop-by-hop headers to the backend. Especially
	// important is "Connection" because we want a persistent
	// connection, regardless of what the client sent to us.
	for _, h := range hopHeaders {
		if outreq.Header.Get(h) != "" {
			if !copiedHeaders {
				outreq.Header = make(http.Header)
				copyHeader(outreq.Header, r.Header)
				copiedHeaders = true
			}
			outreq.Header.Del(h)
		}
	}

	if clientIP, _, err := net.SplitHostPort(r.RemoteAddr); err == nil {
		// If we aren't the first proxy, retain prior
		// X-Forwarded-For information as a comma+space
		// separated list and fold multiple headers into one.
		if prior, ok := outreq.Header["X-Forwarded-For"]; ok {
			clientIP = strings.Join(prior, ", ") + ", " + clientIP
		}
		outreq.Header.Set("X-Forwarded-For", clientIP)
	}

	return outreq
}

var skipHeaders = map[string]struct{}{
	"Content-Type":        {},
	"Content-Disposition": {},
	"Accept-Ranges":       {},
	"Set-Cookie":          {},
	"Cache-Control":       {},
	"Expires":             {},
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		if _, ok := dst[k]; ok {
			// skip some predefined headers
			// see https://github.com/mholt/caddy/issues/1086
			if _, shouldSkip := skipHeaders[k]; shouldSkip {
				continue
			}
			// otherwise, overwrite
			dst.Del(k)
		}
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

var hopHeaders = []string{
	"Alt-Svc",
	"Alternate-Protocol",
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Proxy-Connection", // non-standard but still sent by libcurl and rejected by e.g. google
	"Te",               // canonicalized version of "TE"
	"Trailer",          // not Trailers per URL above; http://www.rfc-editor.org/errata_search.php?eid=4522
	"Transfer-Encoding",
	"Upgrade",
}
