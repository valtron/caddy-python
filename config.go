package python

import (
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func ParseConfig(c *caddy.Controller) ([]*PythonProxy, error) {
	cfg := httpserver.GetConfig(c)
	root := cfg.Root
	
	var proxies []*PythonProxy
	var proxy *PythonProxy
	last := ""
	
	for c.Next() {
		val := c.Val()
		lastTmp := last
		last = ""
		
		switch lastTmp {
			case "python":
				proxy = &PythonProxy {
					Path: val,
					Except: []string{},
					PythonBin: "python",
					Dir: root,
					Processes: []*Process{},
					MaxProcesses: 3,
				}
				proxies = append(proxies, proxy)
			case "except":
				proxy.Except = append(proxy.Except, val)
				proxy.Except = append(proxy.Except, c.RemainingArgs()...)
			case "venv":
				proxy.UseVenv = true
				proxy.PythonBin = proxy.Dir + "/bin/python"
			case "module":
				proxy.Module = val
			case "without":
				proxy.WithoutPrefix = val
			default:
				last = val
		}
	}
	
	return proxies, nil
}
