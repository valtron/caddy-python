package python

import (
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func init() {
	caddy.RegisterPlugin("python", caddy.Plugin {
		ServerType: "http", Action: setup,
	})
}

func setup(c *caddy.Controller) error {
	proxies, err := ParseConfig(c)
	
	if err != nil {
		return err
	}
	
	c.OnShutdown(func () error {
		for _, proxy := range proxies {
			proxy.Close()
		}
		return nil
	})
	
	mid := func (next httpserver.Handler) httpserver.Handler {
		return Handler {
			Next: next, Proxies: proxies,
		}
	}
	
	httpserver.GetConfig(c).AddMiddleware(mid)
	
	return nil
}
