package python

import (
	"testing"
	"github.com/stretchr/testify/assert"
	
	"github.com/mholt/caddy"
)

func TestParseConfigs(t *testing.T) {
	type TestCase struct {
		input    string
		expected []*Config
	}
	
	testcases := []TestCase {
		{ "python /", []*Config { &Config {
			Path: "/",
			Except: []string{},
		} } },
		{ `python /bar {
				module wsgi:app
				without /bar
				except /foo /oof
				venv
			}`, []*Config {
				&Config {
					Path: "/bar",
					Module: "wsgi:app",
					WithoutPrefix: "/bar",
					Except: []string{ "/foo", "/oof" },
					UseVenv: true,
		} } },
		{ `python /first {
				except /not1
			}
			python /second/path {
				venv
			}`, []*Config {
				&Config {
					Path: "/first",
					Except: []string{ "/not1" },
				},
				&Config {
					Path: "/second/path",
					Except: []string{},
					UseVenv: true,
		} } },
	}
	
	for _, test := range testcases {
		controller := caddy.NewTestController("http", test.input)
		actual, err := ParseConfigs(controller)
		if err != nil {
			t.Errorf("ParseConfigs return err: %v", err)
		}
		assert.Equal(t, test.expected, actual)
	}
}
