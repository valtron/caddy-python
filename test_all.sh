#!/bin/sh

set -e

go test -v -covermode=count -coverprofile=coverage.out
